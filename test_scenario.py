from scenario import Scenario, RRPair

rrp = RRPair('one', 'two')
sc = Scenario('test',False)

sc.append_rr_pair(rrp)
print(sc.respond('one'))

sc.load_from('test.txt')

for rrp in sc.get_rr_pairs():
    print(rrp)

for w in ['Hello', 'Goodbye', 'Goodbye\n']:
    print(sc.respond(w))
