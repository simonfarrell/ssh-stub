FROM python:3.9-alpine
RUN apk add --virtual .install_dependencies_paramiko \
    gcc \
    musl-dev \
    python3-dev \
    libffi-dev \
    openssl-dev \
    build-base \
    py-pip \
&&  apk add zlib \
    zlib-dev \
    libssl1.1 \
    openssl-dev \
&&  pip install cffi \
&&  pip install paramiko \
&&  apk del .install_dependencies_paramiko

RUN mkdir -p /app/scenarios
WORKDIR /app
COPY *.py /app/
COPY *.key /app/
COPY scenarios/ /app/scenarios/
CMD ["python", "main.py"]