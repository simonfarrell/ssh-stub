# SSH Stub
This project runs a stub SSH server with configurable responses. 

## Installation
### As a Python application
- The code requires Python 3
- Clone this repository
- Install paramiko by running ```pip install paramiko```. For more details, follow the instructions on the [paramiko website](https://www.paramiko.org/installing.html). It is important to ensure you have all prerequisites installed.

### As a Docker image
By default, the image listens on container port 22 - map this to a port of your choice.
```
docker run -it --rm simonf/ssh-stub
```

## Usage
The Git project and the container both ship with a minimal configuration in the ```scenarios``` directory. 

When the application runs, it starts an SSH server on port 22. The server will accept username/password - based logins. Only the username is important. It will be used to load a *scenario* text file whose name (excluding suffix) matches the username. The contents of the text file are described below.

To quit the application, enter 
```
quit
```
on a line of its own.

## Configuration
The *scenario* text file that matches the login contains input/output pairs, one per line. The input component of each line in the text file will be matched against the SSH client input. If a match is found, the output will be returned. If no match is found, the application will return ```Unrecognised command```. An empty input will simply return a new prompt.

Each line is of the format:
```
input@output[!prompt]
```
Where *input* and *output* can be any strings that do not contain an @ symbol. The optional *prompt* string (which must be preceded by an exclamation mark) will change the prompt displayed to the ssh client.

### Example
The file below will return *Goodbye* if the input is *Hello*. The prompt will then change from the default **>** to **>>** 
```
Hello@Goodbye!>>
```

### Advanced Configuration 

*Output* strings that start with an @ symbol will be treated as filenames. Files must reside in the ```scenarios``` directory. For example, this line will return the contents of ```scenarios/config.txt```:
```
show running config@@config.txt
```

### Docker volumes
To manage per-user configuration files under Docker, map a local directory to the container's ```/app/scenarios``` directory like this example, which maps the content of ```$HOME/ssh-scenarios``` to that directory:
```
docker run -it --rm -p 2200:22 -v $HOME/ssh-scenarios:/app/scenarios simonf/ssh-stub
```

