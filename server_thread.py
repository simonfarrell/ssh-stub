# import base64
from binascii import hexlify
from threading import Thread
import traceback
import paramiko
from paramiko.py3compat import b, u
import sys
from scenario import Scenario
from ssh_server import Server
import char_handler

class ServerThread(Thread):
  def __init__(self, socket):
    Thread.__init__(self)
    self.host_key = paramiko.RSAKey(filename="server_rsa.key")
  # host_key = paramiko.DSSKey(filename='test_dss.key')
    print("Read key: " + u(hexlify(self.host_key.get_fingerprint())))
    self.socket = socket
    print("Created new ServerThread")

  def handle_char(self, chan, inp, c):
    chan.send(c)
    if char_handler.is_backspace(c):
      char_handler.send_backspace_seq(chan)
      return inp[:-1]
    if char_handler.is_eol(c):
      return inp
    else:
      return inp + c.decode()

  def handle_input(self, chan, f):
    # Accumulate and echo characters until end of line
    # Read characters from the file stream f and send on chan
    inp = ''
    while True:
      c = f.read(1)
      inp = self.handle_char(chan, inp, c)
      if char_handler.is_eol(c):
        break
    return inp.strip('\r\n')

  def setup_server(self):
    t = paramiko.Transport(self.socket)
    t.load_server_moduli()
    t.add_server_key(self.host_key)
    server = Server()
    try:
        t.start_server(server=server)
        # wait for auth
        chan = t.accept(20)
        if chan is None:
            print("*** No channel.")
            t.close()
            return (None, None, None)
        else:
          server.event.wait(10)
          if not server.event.is_set():
              print("*** Client never asked for a shell.")
              t.close()
              return(None, None, None)
          else:
            return (t, chan, server.username)
    except paramiko.SSHException:
        print("*** SSH negotiation failed.")
        t.close()
        return (None, None, None)

  def run(self):
    (t, chan, username) = self.setup_server()
    if chan and username:
      self.scenario = Scenario(user=username)
      if not self.scenario.load_from_file():
        chan.send("Unrecognised username\r\n")
        chan.close()
        t.close()
        return
      chan.send("Welcome " + username + "\r\n" + self.scenario.prompt + " ")
      f = chan.makefile("rU")
      while True:
        cmd = self.handle_input(chan, f)
        if cmd == 'quit':
          chan.close()
          t.close()
          return
        elif cmd =='':
          chan.send("\r\n" + self.scenario.prompt + " ")
        else:
          chan.send("\r\n" + self.scenario.respond(cmd))
          chan.send("\r\n" + self.scenario.prompt + " ")

