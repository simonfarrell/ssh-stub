import os
import socket
import sys
import threading
import traceback
from server_thread import ServerThread

class Listener:
  def __init__(self, port):
    self.port = port
    self.threads = []
  
  def listen(self):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(("", self.port))
        sock.listen(100)
        print("Listening for connection ...")
        while True:
            client, addr = sock.accept()
            t = ServerThread(client)
            # t.setDaemon(1)
            t.start()
            self.threads.append(t)
    except Exception as e:
        print("*** Something failed: " + str(e))
        traceback.print_exc()
    finally:
        for t in self.threads:
          t.join()
