def is_backspace(c):
  return c == b'\x7f'

def send_backspace_seq(chan):
  chan.send(b'\x1b[2D')
  chan.send(' ')
  chan.send(b'\x1b[1D')

def is_eol(c):
  return c == b'\n' or c == b'\r'
