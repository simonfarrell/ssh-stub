import os
class Scenario:
    def __init__(self, user="default", dir="scenarios", case_sensitive = False, line_end = "\r\n"):
        self.name = user
        self.dir = dir
        self.rr_pairs = []
        self.case_sensitive = case_sensitive
        self.line_end = line_end
        self.prompt = '>'
    
    def load_from_file(self, fname=None):
        self.rr_pairs = []
        if fname == None:
            fname = self.dir + os.path.sep + self.name + ".txt"
        try:
            f = open(fname, 'r')
            for line in f:
                rrp = RRPair(self.parse_line(line))
                self.append_rr_pair(rrp)
            f.close()
            return True
        except FileNotFoundError:
            print("File not found")
            return False

    def parse_line(self, line):
        return line.split('@',1)

    def get_rr_pairs(self):
        return self.rr_pairs

    def set_rr_pairs(self, rrps):
        self.rr_pairs = rrps
    
    def append_rr_pair(self, rrp):
        self.rr_pairs.append(rrp)

    def respond(self, req):
        for rrp in self.rr_pairs:
            if rrp.is_request(req, self.case_sensitive):
                response = rrp.get_response()
                if response.startswith('@'):
                    return self.load_response_file(response).strip('\r\n')
                else:
                    if rrp.new_prompt:
                        self.prompt = rrp.new_prompt
                    return response.strip('\r\n')
        return "Unrecognised command\r\n"

    def load_response_file(self, s):
        fname = self.dir + os.path.sep + s.strip('@\r\n')
        if not os.path.isfile(fname):
            return '** Missing response file: ' + fname +' **\r\n'
        else:
            with open(fname,'r') as f:
                return '\r\n'.join([n.strip('\r\n') for n in f.readlines()])

class RRPair:
    def __init__(self, *args):
        self.new_prompt = None
        if len(args) == 2:
            self.request = args[0].strip()
            self.set_response(args[1].strip())
        elif len(args) == 1:
            self.request = args[0][0].strip()
            self.set_response(args[0][1].strip())
        else:
            raise TypeError("Bad request/response definition: " + ','.join(args))

    def __str__(self):
        return self.request + " @ " + self.response

    def is_request(self, req, case_sensitive = False):
        if case_sensitive:
            return self.request == req.strip()
        else:
            return self.request.lower() == req.lower().strip()

    def set_response(self, txt):
        if '!' in txt and not txt.endswith('!'):
            ss = txt.split('!')
            self.response = ss[0]
            self.new_prompt = ss[1]
        else:
            self.response = txt
            self.new_prompt = None

    def get_response(self):
        return self.response